package test.http2;

import static org.junit.jupiter.api.Assertions.*;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpVersion;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import java.util.Map;
import javax.inject.Inject;
import org.junit.jupiter.api.Test;

@MicronautTest
class BlahControllerTest {

  @Inject
  @io.micronaut.http.client.annotation.Client(value = "/", httpVersion = HttpVersion.HTTP_2_0)
  RxHttpClient client;

  @Test
  void test() {
    final HttpResponse<String> response = client.toBlocking().exchange("/hello", String.class);
    System.out.println(response.getBody().get());
//    final String response2 = client.toBlocking().retrieve(HttpRequest.GET("/hello"));
//    System.out.println(response2);
  }

}